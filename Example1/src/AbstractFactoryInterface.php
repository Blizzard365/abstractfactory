<?php

declare(strict_types=1);



interface AbstractFactoryInterface
{
   public function createDeliveryService(): DeliveryServiceInterface;
   public function createContainer(): ContainerInterface;
}





