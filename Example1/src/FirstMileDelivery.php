<?php

declare(strict_types=1);



class FirstMileDelivery implements DeliveryServiceInterface
{
   public function deliver(ContainerInterface $container): void
   {
    echo "Доставка контейнера по услуге первая миля";
   }
}





