<?php

declare(strict_types=1);



 class LastMileFactory implements AbstractFactoryInterface
{
   public function createDeliveryService(): DeliveryServiceInterface
   {
        return new LastMileDelivery;
   }

   public function createContainer(): ContainerInterface
   {
        return new LastMileContainer;
   }
}





