<?php

declare(strict_types=1);



 class EmptyMileFactory implements AbstractFactoryInterface
{
   public function createDeliveryService(): DeliveryServiceInterface
   {
        return new EmptyMileDelivery;
   }

   public function createContainer(): ContainerInterface
   {
        return new EmptyMileContainer;
   }
}





