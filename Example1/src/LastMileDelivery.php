<?php

declare(strict_types=1);



class LastMileDelivery implements DeliveryServiceInterface
{
   public function deliver(ContainerInterface $container): void
   {
    echo "Доставка контейнера по услуге последняя миля";
   }
}





