<?php

declare(strict_types=1);



class EmptyMileDelivery implements DeliveryServiceInterface
{
   public function deliver(ContainerInterface $container): void
   {
    echo "Доставка контейнера по услуге порожная миля";
   }
}





