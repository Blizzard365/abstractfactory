<?php

declare(strict_types=1);



interface DeliveryServiceInterface
{
   public function deliver(ContainerInterface $container);
}





