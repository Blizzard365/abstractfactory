<?php

declare(strict_types=1);



 class FirstMileFactory implements AbstractFactoryInterface
{
   public function createDeliveryService(): DeliveryServiceInterface
   {
        return new FirstMileDelivery;
   }

   public function createContainer(): ContainerInterface
   {
        return new FirstMileContainer;
   }
}





