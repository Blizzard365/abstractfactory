<?php

declare(strict_types=1);

require __DIR__ . '../../autoload.php';

function delivery(array $factories){
    foreach($factories as $factory){
        // Получаем сервис доставки
        $deliveryService = $factory->createDeliveryService();
        // Получаем контейнер
        $container = $factory->createContainer();
        // Проверяем контейнер
        $container->getConsist();
        // Отправляем контейнер
        $deliveryService->deliver($container);
    }
}

$factories = [
    new FirstMileFactory(),
    new LastMileFactory(),
    new EmptyMileFactory(),
];

delivery($factories);




